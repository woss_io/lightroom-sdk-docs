const fs = require('fs')
const path = require('path')
const cheerio = require('cheerio')
const debug = require('debug')('formatter')

const modulesPath = path.join(__dirname, '../APIReference/modules')

let structure =  {
    Modulename: {
        description: '',
        types: ['namespace', 'class'],
        functions: {
            functionName: {
                args: {
                    name: {
                        type: '',
                        required: false,
                        description: '',
                        example: '',
                    },
                }, 
                description: '',
                returns: {
                    returnName : {
                        type: 'primitive or LrNamespaceClass',
                        description: ''
                    }
                }
            }
        },
        properties: {}
    }
}

fs.readdir(modulesPath, (err, allFiles) => {
    if (err) {
        console.error(err)
        return
    }
    const files = allFiles.slice(0, 1)

    files.forEach((file) => {
        fs.readFile(path.join(modulesPath, file), { encoding: 'utf8' }, (err, data) => {
            debug('Processing %s', file)

            if (err) {
                console.error(err)
                return
            }
            let moduleStructure = {}
            const $ = cheerio.load(data)
            const content = $('#content')
            
            let title = content.find('h1').text()
            title = title.split('\n')
            const types = title[0].split(' and ')
            const description = content.find('div.module_description').text()
            
            let functions = {}
            const functionList =  content.find('dl.functions')
            console.log(functionList.children())
            functionList.children().each((index, func) => {
                console.log(func)
                let funcTitle = func.find('dt')
                console.log(funcTitle)
            })





            moduleStructure[title[1]] = {
                types,
                description
            }
            
            console.log(moduleStructure)

            
            // fileStructure[title[1]] = {

            // }
            // console.log(title)
            // debug('%j',fileStructure)

        })
    })
})